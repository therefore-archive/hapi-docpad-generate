// Declare internals
var internals = {},
    Bcrypt = require('bcrypt'),
    shell = require('shelljs'),
    _ = require('lodash');

// Defaults
internals.defaults = {};

exports.register = function (plugin, options, next) {

    plugin.hapi.utils.assert(typeof plugin.route === 'function', 'Plugin permissions must allow route');

    var settings = plugin.hapi.utils.applyToDefaults(internals.defaults, options),
        users = options.users,
        path = (options.path !== undefined) ? options.path : '/regenerate',
        server = plugin.servers[0];

    server.app.generateInProgress = false;

    /* Init socket.io */
    require('./socket/socket')(server, users);

    var validate = function (username, password, callback) {

        var user = users[username];

        if (!user) {
            return callback(null, false);
        }

        Bcrypt.compare(password, user.password, function (err, isValid) {
            callback(err, isValid, {id: user.id, name: user.name});
        });
    };

    var socketEnabled = server.app.socketEnabled,
        logger = (socketEnabled && typeof(server.methods.logToSocket) === 'function') ? server.methods.logToSocket : _.noop;

    var logToSocketFiltered = function (string, prependString) {
        var output = (_.isString(string) ? string : string.toString()).replace(/\n$/, '');
        prependString = prependString || '';

        if (!output.match(/(\(\d*ms\))$|12factor-dotenv|DocPad|Contribute:|Plugins:|Shutting down|Environment:|Version:/)) {
            logger(prependString + output.replace(/\[\d\dm.*\[\d\dm/, ''));
        }
    };

    /**
     * Docpad Regeneration
     * @param  {obj} docpad Docpad instace
     */
    var docpadRegen = function (docpad, next) {
        var trigger = 'Manual';

        if (server.app.generateInProgress) {
            logger('warning: Site Generation is already in progress.  Please try again after it completes.');
            return next({status: 'in progress'});
        }
        else {
            server.app.generateInProgress = true;
        }

        logger('info: ' + trigger + ' Regeneration Triggered');
        docpad.log(trigger + ' Regeneration Triggered');

        var regenerateOptions = {
            cache: false,
            initial: false,
            reset: false,
            populate: true,
            reload: false,
            partial: true
        };

        function hook_stdout(callback) {
            var old_write = process.stdout.write;

            process.stdout.write = (function(write) {
                return function(string, encoding, fd) {
                    write.apply(process.stdout, arguments);
                    callback(string, encoding, fd);
                };
            })(process.stdout.write);

            return function() {
                process.stdout.write = old_write;
            };
        }

        var unhook = hook_stdout(function(string, encoding, fd) {
            logToSocketFiltered(string, 'info: ');
        });

        docpad.action('generate', regenerateOptions, function (err, result) {
            unhook();

            if (err) {
                docpad.error(err);
            }

            server.app.generateInProgress = false;
            logger('info: ' + trigger + ' Regeneration Completed');
            docpad.log(trigger + ' Regeneration Completed');
        });

        return next({status: 'started'});
    };

    /**
     * Static regeneration
     * @param  {string}   trigger i.e "Initial", "Manual", "Scheduled"
     * @param  {Function} next    callback
     */
    var staticRegen = function (trigger, next) {
        var isInitial = (trigger === 'Initial');

        next = (typeof(next) === 'function') ? next : _.noop;

        if (server.app.generateInProgress) {
            logger('warning: Site Generation is already in progress.  Please try again after it completes.');
            return next({status: 'in progress'});
        }
        else {
            server.app.generateInProgress = true;
        }

        var executable = (server.app.isPreview ? 'NODE_ENV=preview ' : '') + (!isInitial ? 'SKIP_GULP=1 ' : '') +'SIMPLE_LOGGING=1 docpad generate --env static';
        var child = shell.exec(executable, {async:true});

        logger('info: ' + trigger + ' Regeneration Triggered');

        child.stdout.on('data', function(data) {
            logToSocketFiltered(data);
        });

        child.on('error', function(data) {
            server.app.generateInProgress = false;
            return next({status: 'error', message: data});
        });

        child.on('close', function (code, signal) {
            if (code !== 0) {
                server.app.generateInProgress = false;
                logger('error: ' + trigger + ' Regeneration Failed');
                return next({status: 'error', message: 'Regeneration Failed.'});
            }

            server.app.generateInProgress = false;
            logger('info: ' + trigger + ' Regeneration Completed');
            return next({status: 'complete'});
        });

        if (!socketEnabled || !isInitial) {
            return next({status: 'started'});
        }

    };

    // Add staticRegen as a server method
    server.method('staticRegen', staticRegen);

    server.pack.require('hapi-auth-basic', function (err) {

        if (err) console.log(err);

        plugin.auth.strategy('simple', 'basic', {validateFunc: validate});

        plugin.route({
            method: 'POST',
            path: path,
            handler: function (request, reply) {
                var docpad = request.server.app.docpad;

                if (docpad) {
                    docpadRegen(docpad, reply);
                }
                else {
                    server.methods.staticRegen('Manual', reply);
                }

            },
            config: {
                auth: 'simple'
            }
        });

        next();
    });
};
