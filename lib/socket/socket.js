var _ = require('lodash'),
    Bcrypt = require('bcrypt');

module.exports = function (server, users) {
    /* Create the socket.io connection */
    var io = require("socket.io")(server.listener);
    server.app.socketEnabled = true;

    var validate = function (username, password, callback) {
        var user = users[username];

        if (!user) {
            return callback(null, false);
        }

        Bcrypt.compare(password, user.password, function (err, isValid) {
            callback(err, isValid, {id: user.id, name: user.name});
        });
    };

    var handShake = function (request, callback) {
        var query = request._query
            reject = false;

        if (_.isUndefined(query) || _.isUndefined(query.username) || _.isUndefined(query.password)) {
            return callback('Credentials were not supplied.');
        }
        else {
            var username = query.username,
                password = query.password;

            validate(username, password, function (err, isValid, credentials) {
                if (err || !isValid) return callback('Credentials were not supplied or are incorrect.');
                callback();
            })
        }
    };

    io.use(function(socket, next) {
      // Check Authentication for socket connection attempts
      handShake(socket.request, function (err) {
          if (err) {
            return next(new Error('not authorized'));
          }
          // Authorization successful
          next()
      });
    });

    /* Socket.io logging */
    var logToSocket = function (data) {
        out = {
            status: data.substr(0, data.indexOf(': ')),
            message: data.substr(data.indexOf(': ')+1)
        };

        io.emit('log', out);
    };

    server.method('logToSocket', logToSocket);
};
