# hapi-docpad-generate

Triggers docpad regeneration at a given hapi route

## Getting Started
Install **hapi-docpad-generate** by either running `npm install hapi-docpad-generate` in your sites working directory or add 'hapi-docpad-generate' to the dependencies section of the 'package.json' file and run npm install.

### Required permissions
**hapi-docpad-generate** requires the following permissions to be granted on the server for the plugin to work correctly:
   - route





### Available options
_(Coming soon)_